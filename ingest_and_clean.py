import requests

from tqdm import tqdm
from pathlib import Path
from io import StringIO
import numpy as np
from functools import lru_cache
from bucket import bucket_link

OLD_CACHE = "/home/elmus/floodmap/data_dump"

def load_degree_chuck(index, index2):
   res = requests.post(f'https://topex.ucsd.edu/cgi-bin/get_data.cgi', data= dict(north=30*(index2-2), south=30*(index2-3), east=index+1, west=index, mag=1))
   return res.text

@lru_cache(maxsize=2048)
def load_degree_chunk_cached(long_index, lat_index):
    old_path = Path(f"{OLD_CACHE}/{long_index}_{lat_index}.txt")
    if old_path.exists():
        with old_path.open() as file:
            data = np.loadtxt(file)
    else:
        with StringIO(load_degree_chuck(long_index, lat_index)) as file:
            data = np.loadtxt(file)
    return data

def injest_elevation(long_index, lat_index, gran=1):
    loc = bucket_link("elevation_clean", long=long_index, lat=lat_index, gran=gran)
    if loc.exists():
        return
    data = load_degree_chunk_cached(long_index, lat_index)
    data = lower_granularity(data, gran)
    with loc.open("wb") as file:
        file.write(data.tobytes())
        
def lower_granularity(elevation, seconds):
    degrees = seconds/3600
    coords = elevation[:, :2]
    coords = coords / degrees
    coords = coords.astype(np.int64)
    _,u = np.unique(coords, return_index=True, axis=0)
    return elevation[u, :]
    
def create_archives_chunk(long_chunk, sz=6, gran=600):
    for longitude in tqdm(range(long_chunk, long_chunk+sz), desc=f"chunk {long_chunk}"):
        for lat_chunk in range(6):
            injest_elevation(longitude, lat_chunk, gran)
            
if __name__ == "__main__":
    create_archives_chunk(0, 360, gran=600)