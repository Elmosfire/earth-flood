import numpy as np
from pathlib import Path
from bucket import bucket_link
from scipy.interpolate import NearestNDInterpolator
from tqdm import tqdm

def load_elevation(min_long, max_long):
    data = []
    for long in range(min_long, max_long):
        for lat in range(6):
            chunk = bucket_link("elevation_clean", long=long, lat=lat, gran=600)
            with chunk.open("rb") as file:
                frame = np.frombuffer(file.read()).reshape(-1, 3)
            data.append(frame)
    
    return np.vstack(data)

def build_grid(min_long, rows_per_degree):
    link = bucket_link("elevation_chunk", rpd=rows_per_degree,long=min_long)
    if link.exists():
        return
    max_long = min_long + 6
    elevation = load_elevation(min_long, max_long)
    nip = NearestNDInterpolator(elevation[:, :2], elevation[:, 2])
    
    X = np.arange(min_long, max_long, 1/rows_per_degree)
    Y = -np.arange(-90, 90, 1/rows_per_degree)
    X, Y = np.meshgrid(X, Y)
    
    Z = nip(X, Y)
    
    with link.open("wb") as file:
        file.write(Z.tobytes())
    return Z
    
    
for x in tqdm(range(60)):
    build_grid(x*6, 6)