from google.cloud import storage
from google.oauth2 import service_account
import yaml

with open("file_links.yml") as file:
    CONFIG = yaml.safe_load(file)

def get_bucket_connector():
    credentials = service_account.Credentials.from_service_account_file(
    "key.json", scopes=["https://www.googleapis.com/auth/cloud-platform"],
    )
    client = storage.Client(credentials=credentials, project=credentials.project_id,)
    return client.bucket("oil-blood-flood-cache")

bucket = get_bucket_connector()

def general_file_handler(struct, *legacy_structs, **kwargs):
    fname = struct.format(**kwargs)
    if not bucket.blob(fname).exists() and legacy_structs:
        for legacy_struct in legacy_structs:
            fname_legacy = legacy_struct.format(**kwargs)
            if bucket.blob(fname_legacy).exists():
                bucket.rename_blob(bucket.blob(fname_legacy), fname)
                break
    return bucket.blob(fname)
    

def bucket_link(type_, **kwargs):
    return general_file_handler(
        *CONFIG[type_],
        **kwargs
    )

