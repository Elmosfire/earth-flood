import numpy as np
import matplotlib.pyplot as plt
from bucket import bucket_link
import tqdm 

data = []
for long in tqdm.tqdm(range(0, 360, 6)):
    link = bucket_link("elevation_chunk", rpd=6, long=long)
    if link.exists():
        with link.open("rb") as file:
            data.append(np.frombuffer(file.read()).reshape(1080,-1).T)

    
a = np.vstack(data).T > 0
    
plt.imshow(a, cmap='hot', interpolation='nearest')
plt.legend()
plt.colorbar()
plt.axis("equal")
plt.show()
